import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import postsReducer from './modules/post/postSlice'
import usersReducer from './modules/user/userSlice'
import { apiSlice } from './modules/api/apiSlice'
import authReducer from './modules/auth/authSlice'

export const store = configureStore({
  reducer: {
    posts: postsReducer,
    users: usersReducer,
    [apiSlice.reducerPath]: apiSlice.reducer,
    auth: authReducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(apiSlice.middleware),
  devTools: true
})
