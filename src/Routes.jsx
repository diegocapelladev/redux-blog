import { Route, Routes } from 'react-router-dom'
import RequireAuth from './components/RequireAuth'
import DefaultLayout from './Layout/DefaultLayout'
import Home from './pages/Home'
import Login from './pages/Login'
import Post from './pages/Posts'
import AddPost from './pages/Posts/components/AddPost'
import EditPost from './pages/Posts/components/EditPost'

export const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<DefaultLayout />}>
        {/* Public Routes */}
        <Route path="*" element={<Home />} />
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/post/:postId" element={<Post />} />

        {/* Protected Routes */}
        <Route path="post" element={<RequireAuth />}>
          <Route path="edit/:postId" element={<EditPost />} />
          <Route path="add" element={<AddPost />} />
        </Route>
      </Route>
    </Routes>
  )
}
