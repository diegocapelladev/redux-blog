import { useSelector } from 'react-redux'
import { Link, useParams } from 'react-router-dom'
import { PencilLine } from 'phosphor-react'

import PostAuthor from '../../components/PostAuthor'
import TimeAgo from '../../components/TimeAgo'
import ReactionButtons from '../../components/ReactionButtons'
import { selectPostById } from '../../store/modules/post/postSlice'
import * as S from './styles'

const Post = () => {
  const { postId } = useParams()

  const post = useSelector((state) => selectPostById(state, Number(postId)))

  if (!post) {
    return (
      <S.Section>
        <h2>Post not found!</h2>
      </S.Section>
    )
  }

  return (
    <>
      <S.TitleDetails>
        <h1>Post Details</h1>
      </S.TitleDetails>
      <S.Wrapper>
        <S.Content>
          <S.Title>{post.title}</S.Title>
          <S.Description>{post.body}</S.Description>
        </S.Content>

        <S.Info>
          <PostAuthor userId={post.userId} />
          <TimeAgo timestamp={post.date} />
        </S.Info>

        <ReactionButtons post={post} />
        <Link to={`/post/edit/${post.id}`}>
          <PencilLine size={22} />
        </Link>
      </S.Wrapper>
    </>
  )
}

export default Post
