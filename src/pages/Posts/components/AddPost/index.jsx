import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { addNewPost } from '../../../../store/modules/post/postSlice'
import { selectAllUsers } from '../../../../store/modules/user/userSlice'
import * as S from './styles'

const AddPost = () => {
  const users = useSelector(selectAllUsers)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')
  const [userId, setUserId] = useState('')
  const [addRequestStatus, setAddRequestStatus] = useState('idle')

  const onTitleChanged = (e) => setTitle(e.target.value)
  const onContentChanged = (e) => setContent(e.target.value)
  const onAuthorChanged = (e) => setUserId(e.target.value)

  const canSave =
    [title, content, userId].every(Boolean) && addRequestStatus === 'idle'

  const onSavePost = () => {
    if (canSave) {
      try {
        setAddRequestStatus('loading')
        dispatch(addNewPost({ title, body: content, userId })).unwrap()

        setTitle('')
        setContent('')
        setUserId('')
        navigate('/')
      } catch (error) {
        console.error('Failed to save the post!', error)
      } finally {
        setAddRequestStatus('idle')
      }
    }
  }

  const usersOptions = users.map((user) => (
    <option key={user.id} value={user.id}>
      {user.name}
    </option>
  ))

  return (
    <S.Wrapper>
      <h1>Add a New Post</h1>
      <S.Form>
        <S.Label htmlFor="postTitle">Post Title:</S.Label>
        <S.Input
          type="text"
          id="postTitle"
          name="postTitle"
          value={title}
          onChange={onTitleChanged}
          placeholder="Post title"
        />

        <S.Label htmlFor="postAuthor">Author:</S.Label>
        <S.Select
          type="text"
          id="postAuthor"
          name="postAuthor"
          value={userId}
          onChange={onAuthorChanged}
        >
          <option value="">Select...</option>
          {usersOptions}
        </S.Select>

        <S.Label htmlFor="postContent">Content:</S.Label>
        <S.TextArea
          type="text"
          id="postContent"
          name="postContent"
          value={content}
          onChange={onContentChanged}
          cols="30"
          rows="10"
          placeholder="Your message..."
        />

        <S.SaveButton type="button" onClick={onSavePost} disabled={!canSave}>
          Save Post
        </S.SaveButton>
      </S.Form>
    </S.Wrapper>
  )
}

export default AddPost
