import { useState, useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { setCredentials } from '../../store/modules/auth/authSlice'
import { useLoginMutation } from '../../store/modules/api/authApiSlice'

import * as S from './styles'

const Login = () => {
  const dispatch = useDispatch()
  const userRef = useRef()
  const errRef = useRef()
  const [user, setUser] = useState('')
  const [pwd, setPwd] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const navigate = useNavigate()

  const [login, { isLoading }] = useLoginMutation()

  useEffect(() => {
    userRef.current?.focus()
  }, [])

  useEffect(() => {
    setErrMsg('')
  }, [user, pwd])

  const handleSubmit = async (e) => {
    e.preventDefault()

    try {
      const userData = await login({ user, pwd }).unwrap()
      dispatch(setCredentials({ ...userData, user }))
      setUser('')
      setPwd('')
      navigate('/')
    } catch (err) {
      if (!err?.originalStatus) {
        setErrMsg('No Server originalStatus.')
      } else if (err.originalStatus.status === 400) {
        setErrMsg('Missing Username or Password.')
      } else if (err.originalStatus.status === 401) {
        setErrMsg('Unauthorized.')
      } else {
        setErrMsg('Login Failed.')
      }
      errRef.current?.focus()
    }
  }

  const handleUserInput = (e) => setUser(e.target.value)

  const handlePwdInput = (e) => setPwd(e.target.value)

  const content = isLoading ? (
    <S.Loading>Loading...</S.Loading>
  ) : (
    <>
      <S.Error ref={errRef}>{errMsg}</S.Error>
      <S.FormContainer>
        <S.Form onSubmit={handleSubmit}>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            ref={userRef}
            value={user}
            onChange={handleUserInput}
            placeholder="Username"
            autoComplete="off"
            required
          />

          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={pwd}
            onChange={handlePwdInput}
            placeholder="Password"
            required
          />
          <S.SignInButton type="submit">Sign In</S.SignInButton>
        </S.Form>
      </S.FormContainer>
    </>
  )

  return (
    <S.Wrapper>
      <S.PageTitle>Login</S.PageTitle>
      {content}
    </S.Wrapper>
  )
}
export default Login
