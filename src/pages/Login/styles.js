import { darken } from 'polished'
import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 4rem auto;
`

export const PageTitle = styled.h1`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size28};
    margin-bottom: 2rem;
  `}
`

export const Loading = styled.h2`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size24};
  `}
`

export const Error = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.error};
  `}
`

export const FormContainer = styled.div`
  ${({ theme }) => css`
    max-width: 40rem;
    width: 100%;
    background: ${theme.colors.post};
    border-radius: ${theme.border.radius};
    padding: 4rem;

    ${media.lessThan('medium')`
      padding: 2rem;
    `}
  `}
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;

  ${({ theme }) => css`
    label {
      color: ${theme.colors.subtitle};
      font-size: ${theme.font.sizes.size18};
    }

    input {
      font-size: ${theme.font.sizes.size16};
      color: ${theme.colors.subtitle};
      background: ${theme.colors.input};
      border: 1px solid ${theme.colors.border};
      border-radius: ${theme.border.radius};
      padding: 1rem;
      margin-bottom: 2rem;

      &:focus {
        outline: none;
        border: 1px solid ${theme.colors.blue};
      }

      &::placeholder {
        color: ${theme.colors.label};
        font-size: ${theme.font.sizes.size16};
      }
    }
  `}
`
export const SignInButton = styled.button`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size16};
    font-weight: 600;
    background: ${theme.colors.blue};
    color: ${theme.colors.subtitle};
    border: 1px solid ${theme.colors.border};
    border-radius: ${theme.border.radius};
    padding: 1rem;
    cursor: pointer;
    transition: all 0.2s;
    width: 100%;

    &:hover {
      background: ${darken(0.1, `${theme.colors.blue}`)};
    }
  `}
`
