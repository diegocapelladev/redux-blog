import styled from 'styled-components'
import media from 'styled-media-query'

export const Title = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 4rem;
`

export const Wrapper = styled.main`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 3.2rem;
  margin: 4rem auto;
  width: 100%;

  ${media.lessThan('large')`
    grid-template-columns: 1fr;
  `}
`
