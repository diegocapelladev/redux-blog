import { Outlet } from 'react-router-dom'
import { Container } from '../../components/Container'
import Header from '../../components/Header'

const DefaultLayout = () => (
  <>
    <Header />
    <Container>
      <Outlet />
    </Container>
  </>
)

export default DefaultLayout
