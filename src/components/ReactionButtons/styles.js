import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 2rem;

  ${media.lessThan('medium')`
    gap: 1rem;

    button {
      width: 100%;
      padding: 0.5rem;
      gap: 0.25rem;
    }
  `}
`

export const Button = styled.button`
  cursor: pointer;
  padding: 0.8rem 1rem;
  background: transparent;
  border: none;
  transform: all 0.2s;
  display: flex;
  align-items: center;
  gap: 0.5rem;

  ${({ theme }) => css`
    border-radius: ${theme.border.radius};
    color: ${theme.colors.title};
    font-size: ${theme.font.sizes.size16};

    &:hover {
      background: ${theme.colors.label};
    }
  `}
`
