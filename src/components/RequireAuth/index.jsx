import { useSelector } from 'react-redux'
import { Navigate, Outlet, useLocation } from 'react-router-dom'

import { selectCurrentToken } from '../../store/modules/auth/authSlice'
import * as S from './styles'

const RequireAuth = () => {
  const token = useSelector(selectCurrentToken)
  const location = useLocation()

  return (
    <S.Wrapper>
      {token ? (
        <Outlet />
      ) : (
        <Navigate to="/login" state={{ from: location }} replace />
      )}
    </S.Wrapper>
  )
}

export default RequireAuth
