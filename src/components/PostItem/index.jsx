import { Link } from 'react-router-dom'
import PostAuthor from '../PostAuthor'
import ReactionButtons from '../ReactionButtons'
import TimeAgo from '../TimeAgo'
import * as S from './styles'

const PostItem = ({ post }) => {
  return (
    <S.Wrapper>
      <S.Content>
        <S.Title>{post.title}</S.Title>
        <S.Description>
          {post.body.substring(0, 120)}...
          <Link to={`post/${post.id}`}>View Post</Link>
        </S.Description>
      </S.Content>

      <S.Info>
        <PostAuthor userId={post.userId} />
        <TimeAgo timestamp={post.date} />
      </S.Info>

      <ReactionButtons post={post} />
    </S.Wrapper>
  )
}
export default PostItem
