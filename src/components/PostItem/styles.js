import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.article`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size18};
    background: ${theme.colors.post};
    padding: 3.2rem;
    border-radius: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    gap: 2rem
    width: 100%;

    ${media.lessThan('large')`
      gap: 1rem;
      padding: 1.6rem;
    `}
  `}
`
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  gap: 2rem;
  ${({ theme }) => css`
    a {
      text-decoration: none;
      color: ${theme.colors.blue};
      margin-left: 1rem;
    }

    ${media.lessThan('medium')`

      h2 {
        font-size: ${theme.font.sizes.size18};
      }

      p {
        font-size: ${theme.font.sizes.size14};
      }
    `}
  `}
`

export const Title = styled.h2`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size20};
    font-weight: 700;
  `}
`

export const Description = styled.p`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size16};
  `}
`
export const Info = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 2rem 0;

  span {
    ${({ theme }) => css`
    font-style: italic;
    font-size: ${theme.font.sizes.size14};
    color: ${theme.colors.span};
  `}
  }
`
