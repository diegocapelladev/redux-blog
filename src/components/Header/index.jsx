import { Link } from 'react-router-dom'

import logo from '/images/terminal.png'
import * as S from './styles'
import { useSelector } from 'react-redux'
import { selectCurrentToken } from '../../store/modules/auth/authSlice'

const Header = () => {
  const token = useSelector(selectCurrentToken)

  return (
    <S.Wrapper>
      <S.HeaderContainer>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>

        <S.Navbar>
          <Link to="/">Home</Link>
          {token ? (
            <Link to="/post/add">Add Post</Link>
          ) : (
            <Link to="/login">Login</Link>
          )}
        </S.Navbar>
      </S.HeaderContainer>
    </S.Wrapper>
  )
}
export default Header
