import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.header`
  ${({ theme }) => css`
    height: 10.4rem;
    display: flex;
    align-items: ;
    justify-content: center;
    background: ${theme.colors.profile};
  `}
`
export const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  ${({ theme }) => css`
    max-width: ${theme.grid.container};

    ${media.lessThan('large')`
      padding: 0 calc(${theme.grid.gutter} * 2);
    `}

    ${media.lessThan('medium')`
      padding: 0 calc(${theme.grid.gutter} / 2);

      nav {
        gap: 1rem;
        > a {
          font-size: ${theme.font.sizes.size16};
          padding: 0.5rem 1rem;
        }
      }
    `}
  `}
`

export const Navbar = styled.nav`
  display: flex;
  align-items: center;
  gap: 3.2rem;

  a {
    ${({ theme }) => css`
      text-decoration: none;
      color: ${theme.colors.blue};
      font-size: ${theme.font.sizes.size20};
      border-radius: ${theme.border.radius};
      padding: 1rem 2rem;
      transition: all 0.2s ease-in-out;

      &:hover {
        background: ${theme.colors.label};
        color: ${theme.colors.white};
      }
    `}
  }
`
