# Projeto Blog Posts

<img src="./public/images/capa/blog-post-capa-1.png" />

---
<img src="./public/images/capa/blog-post-capa-2.png" />

---
<img src="./public/images/capa/blog-post-capa-3.png" />

---
<img src="./public/images/capa/blog-post-capa-4.png" />

## O que está dentro?

Este projeto utiliza muitas tecnologias como:

### React, Redux, Redux-Toolkit, Styled-components, Axios, Date-fns, Polished, Phosphor Icons  e responsivo

- [Vite](https://vitejs.dev/)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Styled Components](https://styled-components.com/)
- [Plop](https://plopjs.com/)
- [Babel](https://babeljs.io/)

## Começando

Primeiro, instale as dependências:

`npm install`

ou

`yarn`


## Comandos

- `dev`: executa a aplicação em `localhost:3000`
- `build`: cria a versão de compilação de produção
- `generate`: executa o plop para gerar os arquivos do componente. Ex: `yarn generate Main` ou `npm run generate Main`

***
# English - Google translate

This is a boilerplate.

## What is inside?

This project uses lot of stuff as:

### React, Redux, Redux-Toolkit, Styled-components, Axios, Date-fns, Polished, Phosphor Icons  and responsive

- [Vite](https://vitejs.dev/)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Styled Components](https://styled-components.com/)
- [Plop](https://plopjs.com/)
- [Babel](https://babeljs.io/)

## Getting Started

First, install the dependencies:

`npm install`

or

`yarn`

## Commands

- `dev`: runs your application on `localhost:3000`
- `build`: creates the production build version
- `generate`: runs plop to generate the component files. Ex: `yarn generate Main` or `npm run generate Main`

